import {CONSTANTS} from '../actions';

let listId = 2;
let cardId = 5;
const initialState = [
  {
    title: "First list",
    id: `list-${0}`,
    cards: [
      {
        id: `card-${0}`,
        text: "we created a static list and a static card"
      },
      {
        id: `card-${1}`,
        text: "more text to test content"
      }
    ]
  },
  {
    title: "Second list",
    id: `list-${1}`,
    cards: [
      {
        id: `card-${2}`,
        text: "we created a static list and a static card 2"
      },
      {
        id: `card-${3}`,
        text:"more text to test content 2"
      },
      {
        id: `card-${4}`,
        text: "more text to test content 2"
      }
    ]
  },
]


const listReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.ADD_LIST:
      const newList =  {
        title: action.payload,
        id: `list-${listId}`,
        cards: []
      };
      // console.log('action received', action.payload);
      listId += 1;
      return [...state, newList];
    case CONSTANTS.ADD_CARD:
      const newCard = {
        text: action.payload.text,
        id: `card-${cardId}`
      }
      cardId += 1;
      
      const newState = state.map(list => {
        if(list.id === action.payload.listId){
          return {
            ...list,
            cards: [...list.cards, newCard]
          }
        } else{
          return list;
        }
      });

      return newState;
      case CONSTANTS.DRAG_EVENT:

        const {droppableIdStart,  droppableIdEnd, droppableIndexStart, droppableIndexEnd, type} = action.payload;
        const copyState = [...state];

        // dragging list around
        if(type === 'list') {
          const list = copyState.splice(droppableIndexStart, 1);
          copyState.splice(droppableIndexEnd, 0, ...list);
          return copyState;
        }
        // in the same list 
        if(droppableIdStart === droppableIdEnd){
          const list = state.find(list => droppableIdStart === list.id);
           const card = list.cards.splice(droppableIndexStart, 1);
           list.cards.splice(droppableIndexEnd, 0, ...card);
        }
        // other list
        if(droppableIdStart !== droppableIdEnd) {
          // find the list where drag happened
          const listStart = state.find(list => droppableIdStart === list.id);
          
          // pull out the card from the list
          const card = listStart.cards.splice(droppableIndexStart, 1);

          // find the list where drag ended
          const listEnd = state.find(list => droppableIdEnd === list.id);

          //put the card in new list
          listEnd.cards.splice(droppableIndexEnd, 0, ...card);
        }


        return copyState;

    default: 
      return state;
  }
};

export default listReducer