import {CONSTANTS} from '../actions';

export const addList = (text) => {
  return {
    type: CONSTANTS.ADD_LIST,
    payload: text
  };
};

export const sort = (
  droppableIdStart, 
  droppableIdEnd,
  droppableIndexStart,
  droppableIndexEnd,
  droppableId,
  type,
  ) => {
    return {
      type: CONSTANTS.DRAG_EVENT,
      payload: {
        droppableIdStart,
        droppableIdEnd,
        droppableIndexStart,
        droppableIndexEnd,
        droppableId,
        type
      }
    }
  }