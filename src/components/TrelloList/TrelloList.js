import React from 'react';
import TrelloCard from '../TrelloCard/TrelloCard';
import TrelloActionButton from '../TrelloActionButton/TrelloActionButton';
import {Droppable, Draggable} from 'react-beautiful-dnd';
import styled from 'styled-components';

const ListContainer = styled.div`
  position: relative;
  background-color: #dfe3e6;
  border-radius: 3px;
  width: 300px;
  min-width: 300px;
  height: 100%;
  padding: 10px;
  margin: 0px 5px;
`;


const TrelloList = ({listId, title, cards, index, onDelete}) => {
  return (
    <Draggable 
      draggableId={String(listId)} 
      index={index}>
      {provided => (
        <ListContainer 
          {...provided.draggableProps}  
          {...provided.dragHandleProps}
          ref={provided.innerRef}>
          <Droppable droppableId={String(listId)}>
            {(provided) => (
              <div {...provided.droppableProps} ref={provided.innerRef}>
                <h4>{title}</h4>
                {cards.map((card, index) => (
                  <TrelloCard 
                    onDelete={() => onDelete(listId, card.id)} 
                    index={index} 
                    listId={listId} 
                    id={card.id} 
                    key={card.id} 
                    title={card.title} 
                    descr={card.descr}/>
                  ))}
                {provided.placeholder}
                <TrelloActionButton  listId={listId}/>
              </div>
            )}
          </Droppable>
        </ListContainer>
      )}
    </Draggable>
  )
}

export default TrelloList;