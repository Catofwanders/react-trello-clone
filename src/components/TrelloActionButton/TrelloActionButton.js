<<<<<<< HEAD
import React from 'react';
import Icon from '@material-ui/core/Icon';
import Card from '@material-ui/core/Card';
import Textarea from 'react-textarea-autosize';
import { Button } from '@material-ui/core';
import {connect} from 'react-redux';
import {addList, addCard} from '../../actions';
import styled from 'styled-components';

const AddButton = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  border-radius: 3px;
  height: 36px;
  min-width: 300px;
  padding-left: 10px;
`;

const AddForm = styled.div`
  margin-top: 8px;
  display: flex;
  align-items: center;
`;


class TrelloActionButton extends React.Component {
  state = {
    formOpen: false,
    text: ''
  }

  handleInputChange = e => {
    this.setState({
      text: e.target.value
    })
  }

  openCloseForm = () => {
    this.setState(prevState => {
      return {formOpen: !prevState.formOpen};
    })
  }

  renderAddButton = (props) => {
    const {list} = this.props;
    const buttonText = list ? 'Add another list' : 'Add enother card';
    const buttonTextOpacity = list ? 1 : 0.5;
    const buttonTextColor = list ? 'white' : null;
    const buttonTextBackground = list ? 'rgba(0,0,0,.15)' : null;
    return (
      <AddButton
        onClick = {this.openCloseForm}
        style={{
          opacity: buttonTextOpacity,
          color: buttonTextColor,
          background: buttonTextBackground,
        }}>
          <Icon>add</Icon>
          <p>{buttonText}</p>
      </AddButton>
    )
  }

  renderForm = () => {
    const {list} = this.props;

    const placeholder = list ? 'Enter list title...' : 'Enter a title for this card ...';
    const buttonTitle = list ? 'Add list' : 'Add Card';
    return (
      <div 
        style={{
        minWidth: 300,
      }}>
        <Card style={{
          overflow: 'visible',
          minHeight: 80,
          minWidth: 100,
          padding: '6px 8px 2px'
        }}>
          <Textarea 
            placeholder={placeholder} 
            autoFocus 
            onBlur={this.openCloseForm}
            value={this.state.text}
            onChange={this.handleInputChange}
            style={{
              resize: 'none',
              width: '100%',
              boxSizing: 'border-box',
              padding: '4px 10px',
              outline: 'none',
              border: 'none'
            }}
          />
        </Card>
        <AddForm>
          <Button 
            onMouseDown={list ? this.handleAddList : this.handleAddCard}
            variant="contained" 
            style={{
              color: 'white',
              backgroundColor: '#5aac44',
              marginTop: 10
            }}
          >
            {buttonTitle}
          </Button>
          <Icon 
            onClick={this.openCloseForm}
            style={{ marginLeft: 8, cursor: 'pointer' }}>close</Icon>
        </AddForm>
      </div>
    )
  }

  handleAddList = () => {
    const {dispatch} = this.props;
    const {text} = this.state;

    if(text) {
=======
import React, { Component } from "react";
import { Icon, Card, TextareaAutosize, Button } from "@material-ui/core/";
import {connect} from 'react-redux';
import {addList, addCard} from '../../store/actions';

class TrelloActionButton extends Component {

  state = {
    formOpen: false,
    text: ""
  }

  renderAddButton = () => {
    const { list } = this.props;
    const buttonText = list ? "Add another list" : "Add another card";
    const buttonTextOpacity = list ? 1 : 0.5;
    const buttonTextColor = list ? "white" : "inherit";
    const buttonTextBackground = list ? "rgba(0,0,0,.15)" : "inherit";

    return (
      <div onClick={this.formOpenHandler} style={{
          ...styles.addButton,
          opacity: buttonTextOpacity, 
          color: buttonTextColor, 
          background: buttonTextBackground
      }}>
        <Icon>add</Icon>
        <p>{buttonText}</p>
      </div>
    );
  };
  
  renderForm = () => {
    const {list} = this.props;

    const placeholder = list ? "Enter list title" : "Enter title for this card...";

    const buttonTitle = list ? "Add List" : "Add Card";

    return (
      <div>
        <Card style={styles.addCard}>
          <TextareaAutosize 
            style={styles.TextareaAutosize}
            onBlur={this.formOpenHandler}
            placeholder={placeholder}
            autoFocus
            value={this.state.text}
            onChange={this.inputChangeHandler}
            />
        </Card>
        <div style={styles.addButtonWrapper}>
          <Button 
            onMouseDown={list ? this.addListHandler : this.addCardHandler}
            variant="contained" 
            style={styles.addCardButton}>
            {buttonTitle} 
          </Button>
          <Icon
            style={styles.addButtonClose}
            >close</Icon>
        </div>
      </div>
    );
  }

  inputChangeHandler = e => {
    this.setState({text: e.target.value});
  }

  formOpenHandler = () => {
    this.setState({
      formOpen: !this.state.formOpen
    })
  }

  addListHandler = () => {
    const {dispatch} = this.props;
    const {text} = this.state;
    if(text){
>>>>>>> newtree
      this.setState({
        text: ''
      });
      dispatch(addList(text));
    }
<<<<<<< HEAD
  }
  handleAddCard = () => {
=======
    return;
  }

  addCardHandler = () => {
>>>>>>> newtree
    const {dispatch, listId} = this.props;
    const {text} = this.state;
    if(text){
      this.setState({
        text: ''
      });
<<<<<<< HEAD
      dispatch(addCard(text,listId));
    }
  }

  render() {
    return this.state.formOpen ? this.renderForm() : this.renderAddButton();
  }
=======
      dispatch(addCard(listId, text));
    }
  }
  render(){
    return this.state.formOpen ? this.renderForm(): this.renderAddButton();
  }
}

const styles = {
  addButtonClose: {
    cursor: "pointer",
    marginLeft: 8
  },
  addButtonWrapper: {
    display: "flex",
    alignItems: "center",
    marginTop: 8
  },
  addCardButton: {
    color: "white",
    backgroundColor: "#5aac44"
  },
  addCard: {
    oveflow: "visible",
    minHeight: 80,
    minWidth: 272,
    padding: "6px 8px 2px"
  },
  addButton: {
    width: 272,
    height: 40,
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    borderRadius: 8,
    paddingLeft: 10
  },
  TextareaAutosize: {
    width: "100%",
    boxSizing: "border-box",
    padding: 8,
    border: "none",
    resize: "none",
    outline: "none",
    oveflow: "hidden"
  }

>>>>>>> newtree
}

export default connect()(TrelloActionButton);