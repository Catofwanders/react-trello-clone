<<<<<<< HEAD
import React, { Component } from 'react';
import List from '../List/List';
import {connect} from 'react-redux';
import TrelloActionButton from '../TrelloActionButton/TrelloActionButton';
import {DragDropContext, Droppable} from 'react-beautiful-dnd';
import {sort} from '../../actions';
import styled from 'styled-components';
=======
import React, { Component } from "react";
import TrelloList from "../TrelloList/TrelloList";
import {connect} from "react-redux";
import TrelloActionButton from "../TrelloActionButton/TrelloActionButton";
import {DragDropContext, Droppable} from "react-beautiful-dnd";
import styled from 'styled-components';
import {sort, deleteCard} from '../../store/actions';
>>>>>>> newtree

const ListContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

class App extends Component {
  
  onDragEnd = (result) => {
    const {destination, source, draggableId, type} = result;
    if(!destination){
      return;
    }

    this.props.dispatch(sort(
      source.droppableId,
      destination.droppableId,
      source.index,
      destination.index,
      draggableId,
      type
    ))
  }

<<<<<<< HEAD
=======
  
>>>>>>> newtree
  render() {
    const {lists} = this.props;
    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <div className="App">
          <Droppable droppableId="all-lists" direction="horizontal" type="list" >
            {provided=>(
              <ListContainer {...provided.droppableProps} ref={provided.innerRef}>
                {lists.map(({id, title, cards}, index) => (
<<<<<<< HEAD
                  <List 
=======
                  <TrelloList 
>>>>>>> newtree
                    listId={id} 
                    key={id} 
                    title={title} 
                    cards={cards}
                    index={index}
<<<<<<< HEAD
=======
                    onDelete={this.props.onDeleteHandler}
>>>>>>> newtree
                  />
                  ))}
                {provided.placeholder}
                <TrelloActionButton list/>
              </ListContainer>
            )}
          </Droppable>
        </div>
      </DragDropContext>
    );
  }

}

const mapStateToProps = state => ({
  lists: state.lists
});

<<<<<<< HEAD
export default connect(mapStateToProps)(App);
=======
const mapDispatchToProps = dispatch => {
  return{
    onDeleteHandler: (listId, id) => dispatch(deleteCard(listId, id)),
  }
};

export default connect(mapStateToProps,mapDispatchToProps)(App);
>>>>>>> newtree
