import React from 'react';
import TCard from '../Card/Card';
import TrelloActionButton from '../TrelloActionButton/TrelloActionButton';
import {Droppable, Draggable} from 'react-beautiful-dnd';
import styled from 'styled-components';

const ListContainer = styled.div`
  position: relative;
  background-color: #ccc;
  border-radius: 3px;
  width: 300px;
  min-width: 300px;
  height: 100%;
  padding: 10px;
  margin: 0px 5px;
`;

const List = ({title, cards, listId, index}) => {
  return(
    <Draggable draggableId={String(listId)} index={index}>
      {provided => (
        <ListContainer 
          {...provided.draggableProps}  
          {...provided.dragHandleProps}
          ref={provided.innerRef} >
          <Droppable droppableId={String(listId)}>
            {provided => (
                <div {...provided.droppableProps} ref={provided.innerRef}>
                  <h4>{title}</h4>
                  {cards.map((card, index) => (
                    <TCard index={index} id={card.id} key={card.id} content={card.text}/>
                    ))}
                  {provided.placeholder}
                  <TrelloActionButton listId={listId}/>
                </div>
            )}
          </Droppable>
        </ListContainer>
      )}
    </Draggable>
  )
}

export default List;