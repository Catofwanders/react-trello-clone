import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
// import EditIcon from '@material-ui/icons/Edit';
import {Draggable} from 'react-beautiful-dnd';
import styled from 'styled-components';


const EditContainer = styled.div`
  position: absolute;
  right: 8px;
  top: 7px;
  display: none;
  // flex-direction: column;
  align-items: center;
  justify-content: center;

`;
const CardContainer = styled.div`
  position: relative;
  margin-bottom: 8px;
  &:hover ${EditContainer}{
    display: flex;
  }
`;


const TrelloCard = ({title, id, index, listId,onDelete}) => {
  return (
    <Draggable draggableId={String(id)} index={index}>
      {provided => (
        <CardContainer 
          ref={provided.innerRef} 
          {...provided.draggableProps} 
          {...provided.dragHandleProps}>
          <Card>
            <CardContent>
              <Typography variant="body2" component="p">
                {title}
              </Typography>
            </CardContent>
          </Card>
          <EditContainer>
            {/* <IconButton> <EditIcon/> </IconButton> */}
            <IconButton onClick={onDelete}> <DeleteIcon/> </IconButton>
          </EditContainer>
        </CardContainer>
      )}
    </Draggable>
  );
}

export default TrelloCard;