import {CONSTANTS} from './index';

export const addCard = (listId, title) => {
  return {
    type: CONSTANTS.ADD_CARD,
    payload: {listId, title}
  }
}

export const deleteCard = (listId, cardId) => {
  return {
    type: CONSTANTS.DELETE_CARD,
    payload: {listId, cardId}
  }
}