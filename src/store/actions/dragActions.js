import {CONSTANTS} from './index';

export const sort = (
  droppableIdStart, 
  droppableIdEnd,
  droppableIndexStart,
  droppableIndexEnd,
  droppableId,
  type,
  ) => {
    return {
      type: CONSTANTS.DRAG_EVENT,
      payload: {
        droppableIdStart,
        droppableIdEnd,
        droppableIndexStart,
        droppableIndexEnd,
        droppableId,
        type
      }
    }
  }