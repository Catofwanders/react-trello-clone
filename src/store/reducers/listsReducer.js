import {CONSTANTS} from '../actions';
import {updateObject} from '../../shared/utility';

let listId = 2;
let cardId = 5;

const initialState = [
  {
    title: "First list",
    id: `list-${0}`,
    cards: [
      {
        id: `card-${0}`,
        title: "we created a static list and a static card"
      },
      {
        id: `card-${1}`,
        title: "more title to test content"
      }
    ]
  },
  {
    title: "Second list",
    id: `list-${1}`,
    cards: [
      {
        id: `card-${2}`,
        title: "we created a static list and a static card 2"
      },
      {
        id: `card-${3}`,
        title:"more title to test content 2"
      },
      {
        id: `card-${4}`,
        title: "more title to test content 2"
      }
    ]
  }
]

const addList = (state, action) => {
  const newList =  {
    title: action.payload,
    id: `list-${listId}`,
    cards: []
  };
  listId += 1;
  return updateObject(state, newList);
}

const addCard = (state, action) => {
  const newCard = {
    title: action.payload.title,
    id: `card-${cardId}`
  }
  cardId += 1;
  
  const newState = state.map(list => {
    if(list.id === action.payload.listId){
      return {
        ...list,
        cards: updateObject(list.cards, newCard)
      }
    } else{
      return list;
    }
  });

  return newState;
}

const dragEvent = (state, action) => {
  const {droppableIdStart,  droppableIdEnd, droppableIndexStart, droppableIndexEnd, type} = action.payload;
  const copyState = [...state];
  // dragging list around
  if(type === 'list') {
    const list = copyState.splice(droppableIndexStart, 1);
    copyState.splice(droppableIndexEnd, 0, ...list);
    return copyState;
  }
  // in the same list 
  if(droppableIdStart === droppableIdEnd){
    const list = state.find(list => droppableIdStart === list.id);
    const card = list.cards.splice(droppableIndexStart, 1);
    list.cards.splice(droppableIndexEnd, 0, ...card);
  }
  // other list
  if(droppableIdStart !== droppableIdEnd) {
    // find the list where drag happened
    const listStart = state.find(list => droppableIdStart === list.id);
    // pull out the card from the list
    const card = listStart.cards.splice(droppableIndexStart, 1);
    // find the list where drag ended
    const listEnd = state.find(list => droppableIdEnd === list.id);
    //put the card in new list
    listEnd.cards.splice(droppableIndexEnd, 0, ...card);
  }

  return copyState;
}

const deleteCard = (state, action) => {
  const copyState = [...state];
  const {listId, cardId} = action.payload;
  const list = copyState.find(list => listId === list.id);
  list.cards.splice(list.cards.indexOf(list.cards.find(card => card.id === cardId)), 1);
  return copyState;
}

const listReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.ADD_LIST: return addList(state, action);
    case CONSTANTS.ADD_CARD: return addCard(state, action);
    case CONSTANTS.DELETE_CARD: return deleteCard(state, action);
    case CONSTANTS.DRAG_EVENT: return dragEvent(state, action);
    default: return state;
  }
}

export default listReducer;